# HUGO to PanDoc CLI

CLI voor het genereren van:

- PanDoc voorbereide content in R Markdown (en LaTeX) formaat 
- PanDoc configuratie op basis van de HUGO SSG Markdown content als input

## Installatie CLI

Pre-installatie vereisten:

- `NodeJS` geinstalleerd
- `NPM` geinstalleerd

Commando-regel Installatie: 

```shell
npm install https://gitlab.com/istddevops/shared/pandoc/hugo-to-pandoc.git
```

## Gebruik CLI

Zie hieronder standaard gebruik.

```shell
npx hugotopandoc <contentDir> <outputFile> <exportDir>
```

| Parameter | Beschrijving |
|:-|:-|
| `<contentDir>` | Locatie HUGO Markdown content |
| `<outputFile>` | PanDoc configuratie parameter uitvoer-bestand (PDF) |
| `<exportDir>` | Locatie PanDoc voorbereide content en configuratie |

## Voorbereiding PanDoc content

Er wordt vanuit gegaan dat de HUGO Markdown content voldoet aan de [HUGO SSG Markdown Content Vereisten](pandoc-config#hugo-ssg-markdown-content-vereisten). Dit als onderdeel van de [PanDoc Configuratie](pandoc-config#pandoc-configuratie) om te komen tot het juiste de invoer-formaat voor het genereren van een PDF-document.

PM (Flow em Algo)
