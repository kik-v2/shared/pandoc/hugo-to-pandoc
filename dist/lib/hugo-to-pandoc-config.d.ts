interface IndexString {
    [index: string]: string;
}
export declare const panDocDefaultsTemplateFilePath: string;
export declare const panDocLatexTemplateFilePath: string;
export declare const hugoBookDefaultsFile = "hugo-book-defaults.yml";
/**
 * Mappings for conversion from **HUGO Book Theme** (with Template Customizations) **Markdown**
 * that is used to generate the **static HTML-site** to **PanDoc R Markdown** as the basis for
 * generating a **LateX based PDF-document**.
 *
 * From HUGO SHortCodes:
 * - [Built in](https://gohugo.io/content-management/shortcodes/#use-hugos-built-in-shortcodes)
 * - [Book Theme](https://github.com/alex-shpak/hugo-book#shortcodes)
 *
 * To PanDoc Markdown / LateX:
 * - [R Markdown](https://rmarkdown.rstudio.com/authoring_pandoc_markdown.html#Pandoc_Markdown)
 * - LaTeX [raw](https://rmarkdown.rstudio.com/authoring_pandoc_markdown.html#Raw_TeX)
 * and [macros](https://rmarkdown.rstudio.com/authoring_pandoc_markdown.html#LaTeX_macros)
 */
export declare const hugoShortCodeToPanDocMapping: IndexString;
export {};
