"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.hugoShortCodeToPanDocMapping = exports.hugoBookDefaultsFile = exports.panDocLatexTemplateFilePath = exports.panDocDefaultsTemplateFilePath = void 0;
// NodeJs inports
const path_1 = require("path");
const fs_extra_1 = require("fs-extra");
/**
 * PanDoc Default and Template files resolving
 */
const panDocConfigPath = path_1.resolve(__dirname, '..', '..', 'pandoc-config');
exports.panDocDefaultsTemplateFilePath = path_1.resolve(panDocConfigPath, 'defaults.yml');
exports.panDocLatexTemplateFilePath = path_1.resolve(panDocConfigPath, 'template.tex');
exports.hugoBookDefaultsFile = 'hugo-book-defaults.yml';
/**
 * Mappings for conversion from **HUGO Book Theme** (with Template Customizations) **Markdown**
 * that is used to generate the **static HTML-site** to **PanDoc R Markdown** as the basis for
 * generating a **LateX based PDF-document**.
 *
 * From HUGO SHortCodes:
 * - [Built in](https://gohugo.io/content-management/shortcodes/#use-hugos-built-in-shortcodes)
 * - [Book Theme](https://github.com/alex-shpak/hugo-book#shortcodes)
 *
 * To PanDoc Markdown / LateX:
 * - [R Markdown](https://rmarkdown.rstudio.com/authoring_pandoc_markdown.html#Pandoc_Markdown)
 * - LaTeX [raw](https://rmarkdown.rstudio.com/authoring_pandoc_markdown.html#Raw_TeX)
 * and [macros](https://rmarkdown.rstudio.com/authoring_pandoc_markdown.html#LaTeX_macros)
 */
exports.hugoShortCodeToPanDocMapping = fs_extra_1.readJSONSync(path_1.resolve(panDocConfigPath, 'hugo-short-code-to-pandoc-mapping.json'));
