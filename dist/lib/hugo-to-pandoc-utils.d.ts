/**
 * HUGO SSG Utils in TypeScript
 */
/**
 * Export HUGO Book Theme based Content to PanDoc (input ready) Markdown
 * and create PanDoc Defaults File to execute Latex PDF Engine Document
 *
 * @param contentDir HUGO Book Theme content directory
 * @param outputFIle PanDoc LatEX PDF output file location
 * @param exportDir PanDoc pre-processed input directory
 */
export declare function convertHugoBookContentToPanDocMd(contentDir?: string, outputFile?: string, exportDir?: string): void;
/**
 * Prepare HUGO Content Management based Markdown content for PanDoc Md processing
 * and create ordered input file content list and resource paths list
 *
 * @param contentDir directory with HUGO Markdown-content input
 * @param exportDir directory with PanDoc LateX prepared Markdown-content output
 * @returns ordered input content list for PanDoc processing and resource paths (directies with non Markdown-files)
 */
export declare function preProcessHugoBookContentToPanDocMd(contentDir: string, exportDir: string): {
    contentFileList: string[];
    resourcePathList: string[];
};
