---
title: "Afsprakenset"
description: "Afspraken over het opstellen, wijzigen, vaststellen, publiceren en implementeren van afsprakenset."
weight: 1
---

# Ontwikkeling afsprakenset

{{< hint info >}}
{{< param description >}}
{{< /hint >}}

## Proces

- De beheerorganisatie onderhoudt de afsprakenset en richt hiervoor een wijzigingsproces in.
- Deelnemers en de beheerorganisatie kunnen wijzigingsvoorstellen indienen. Een volledig uitgewerkt wijzigingsverzoek bevat:
  - Een beschrijving van het ervaren probleem c.q. de behoefte, inclusief de situatie(s) waarin deze wordt ervaren.
  - Een analyse van de grondoorzaken.
  - De voorgestelde wijzigingen aan de afsprakenset en de impact op de rest van de afspraken.
  - Eventuele alternatieve oplossingen.
  - Een beschrijving van de implicaties voor de implementatie voor de belanghebbende partijen.
  - Een beschrijving van de wijze van implementatie die de minste impact en verstoringen veroorzaakt.
  - Een beschrijving van het draagvlak voor de voorgestelde wijziging.
  - Indien niet evident, een beschrijving van de business case.
- De beheerorganisatie doorloopt bij een wijzigingsverzoek aan de modelgegevensset eerst het [Matchingsproces](../matchingproces) en hanteert vervolgens bij de onderbouwing van een eventueel wijzigingsverzoek het bij dit proces beschreven Afwegingskader uitbreiding modelgegevensset.
- Een wijzigingsverzoek hoeft bij het indienen niet compleet te zijn. De beheerorganisatie ziet toe op de kwaliteit en volledigheid van ingediende wijzigingsvoorstellen door:
  - Indieners van wijzigingen te adviseren en begeleiden;
  - De relatie met de strategische kaders, releasekalender en andere wijzigingsverzoeken te borgen;
  - Te borgen dat voorgestelde wijzigingen voldoen aan vigerende wet- en regelgeving en aansluiten bij de principes van KIK-V;
  - Fora (referentiegroepen) te organiseren waarin tot inhoudelijke afstemming en draagvlak kan worden gekomen. In deze fora worden naast deelnemers, onder andere ook softwareleveranciers betrokken; 
  - Te borgen dat eventueel auteursrecht op een voorgestelde oplossing wordt overgedragen;
  - Eindredactie te voeren over wijzigingen en deze te voorzien van een (onafhankelijk) advies.
- De beheerorganisatie legt de wijzigingsverzoeken gebundeld voor ter besluitvorming.
- De beheerorganisatie onderzoekt, alvorens wijzigingsverzoeken ter besluitvorming worden voorgelegd, de impact op bestaande uitwisselprofielen. De uitkomst wordt meegenomen in de besluitvorming.
- De beheerorganisatie faciliteert de besluitvorming en de daarvoor benodigde informatievoorziening.
- De Ketenraad KIK-V besluit over het al dan niet doorvoeren van (majeure) wijzigingen en ziet toe op de samenhang met de uitwisselprofielen. Het Tactisch Overleg KIK-V besluit over kleine en/of spoedwijzigingen en legt hierover verantwoording af aan de Ketenraad KIK-V.
- De beheerorganisatie is na vaststelling verantwoordelijk voor het verwerken van de wijzigingsverzoeken in een nieuwe publicatie van de afsprakenset.
- De beheerorganisatie publiceert de afsprakenset op een voor aanbieders en afnemers toegankelijke plek. 
- De beheerorganisatie voorziet in de benodigde informatievoorziening richting deelnemers ten behoeve van een soepele implementatie.
- De beheerorganisatie archiveert wijzigingsvoorstellen.
- De beheerorganisatie evalueert in samenspraak met de deelnemers zowel het proces voor de totstandkoming als de inhoud van de nieuwe release. 
- De beheerorganisatie kan, waar nodig en gepast, totstandkoming en onderhoud van voorzieningen faciliteren die een rol spelen bij de gegevensuitwisseling zoals beschreven in de afsprakenset.

De ontwikkeling van wijzigingsverzoeken, bundeling tot releases en vaststelling van deze releases door het Tactisch Overleg en de Ketenraad ziet er in samenhang als volgt uit:

![Wijzigings-proces](wijzigings-proces.png)

## Releasekalender en releases

- Geplande doorontwikkeling van de afsprakenset vindt zoveel mogelijk plaats volgens een vaste releasecyclus en -kalender. 
- De volgende strategische overwegingen spelen minimaal een rol bij de totstandkoming van de strategische releasekalender:
  - Welke aan KIK-V gerelateerde vraagstukken behoeven aandacht en een gemeenschappelijke inspanning van afnemers en aanbieders? Denk aan:
    - Het uitbreiden van de beschikbare gegevens met andere thema’s;
    - Het verbeteren van de kwaliteit van gegevens;
    - Het verduidelijken van de context van gegevens;
    - Het verbeteren van de manier van gegevensuitwisseling;
    - Het uitbreiden naar andere domeinen in de zorg;
    - Het uitbreiden van de toepassingsgebieden van KIK-V;
    - De andere in praktijk ervaren knelpunten.
    - Welke van deze vraagstukken passen binnen de scope van KIK-V?
    - Welke van deze vraagstukken creëren op korte termijn de meeste meerwaarde?
    - Welke van deze vraagstukken zijn op lange termijn voor KIK-V van belang en dienen al tijdig te worden opgepakt om op tijd een oplossing te kunnen bieden?
    - Welke van deze vraagstukken brengen de meeste kans op succes met zich mee gezien de potentiële oplossingsruimte? Denk aan:
      - Politieke aandacht voor het vraagstuk;
      - Beschikbare financiële middelen;
      - Initiatief bij de betrokken partijen;
      - Belangen van invloedrijke stakeholders.
- De beheerorganisatie faciliteert de totstandkoming en vaststelling van een strategische releasekalender (meerjarig en jaarlijks) voor de doorontwikkeling. De releasekalender bevat de belangrijkste mijlpalen en voorgenomen wijzigingen per geplande release van de afsprakenset.
- De afsprakenset kent jaarlijks maximaal één normatieve (majeure) releases.
- De Ketenraad KIK-V besluit over de vaststelling van de strategische releasekalender. Een vertegenwoordiging van deelnemers wordt minimaal geconsulteerd in het besluitvormingsproces.
- Wijzigingsvoorstellen dienen te passen binnen de vastgestelde strategische releasekalender.
- Er mag worden afgeweken van de releasekalender bij:
  - Wijzigingen die nodig zijn om een onmiddellijke dreiging voor de continuïteit van of het vertrouwen in de gegevensuitwisseling binnen KIK-V af te wenden;
  - Verbeteringen waarvan de baten van spoedig doorvoeren significant groter zijn dan de implementatie-inspanningen, en die op breed draagvlak onder de Deelnemers kunnen rekenen.

## Relaties met aanpalende ontwikkelingen en standaarden buiten KIK-V

- De beheerorganisatie ziet actief toe op de relaties met aanpalende ontwikkelingen (bijvoorbeeld veranderende wetgeving) en standaarden buiten KIK-V (zoals ZIB’s), behartigt hierbij de belangen van KIK-V en signaleert relevante wijzigingen. 
- De beheerorganisatie bepaalt de impact van aanpalende ontwikkelingen en standaarden buiten KIK-V op de eigen afsprakenset en komt waar nodig met wijzigingsvoorstellen.
- De beheerorganisatie is verantwoordelijk om het gebruik van de externe standaard te evalueren, zowel procesmatig (bijvoorbeeld de manier waarop externe standaard met wijzigingen omgaat) als inhoudelijk (in hoeverre sluit de standaard nog voldoende aan).
