---
title: "Betrokkenheid aanbieders"
description: "Deze pagina beschrijft de wijze waarop aanbieders worden betrokken bij de ontwikkeling van de afsprakenset en uitwisselprofielen. Hier zijn aanvullende afspraken over nodig vanwege het grote aantal en de diversiteit aan aanbieders."
weight: 4
---

# Betrokkenheid aanbieders bij ontwikkeling

{{< hint info >}}
{{< param description >}}
{{< /hint >}}

## Ontwikkeling afsprakenset

### Doel van betrokkenheid aanbieders

- Toets op de gegevens (juiste definitie) in (of voor opname in) de modelgegevensset.
- Toets op toepasselijkheid modelgegevensset in verschillende situaties bij aanbieders (o.a. afhankelijk van inrichting informatievoorziening en volwassenheidsniveau datagericht werken).
- (mede) Bepalen belang en nut/noodzaak van een onderdeel in het modeluitwisselprofiel.
- Input voor de uitwisselkalender en mede bewaken van de uitwisselkalender.

### In welke stappen

- Bij het indienen van wijzigingsverzoeken. Een aanbieder kan bij de beheerorganisatie wensen aangeven met betrekking tot de afsprakenset of een uitwisselprofiel. ActiZ kan tevens, als deelnemer KIK-V, wijzigingsverzoeken indienen bij de beheerorganisatie KIK-V. 
- In de beoordeling van wijzigingsverzoeken. Indien het wijzigingsverzoek van toepassing is op de model gegevensset doorloopt de beheerorganisatie het Matchingsproces en hanteert vervolgens bij de onderbouwing van een eventueel wijzigingsverzoek het bij dit proces beschreven Afwegingskader uitbreiding modelgegevensset. In het matchingsproces en toepassing van het afwegingskader is betrokkenheid vanuit het perspectief van de aanbieder van belang, met name vanwege de toets op beschikbaarheid van (welke) gegevens in bestaande processen en systemen (dan wel of registratie van bepaalde gegevens een toegevoegde waarde kan hebben). In de beoordeling van wijzigingsverzoeken op andere onderdelen van de afsprakenset kan afstemming met zorgaanbieders van belang zijn. 
- Bij de inhoudelijke afstemming over de gebundelde en in samenhang gepresenteerde wijzigingsverzoeken.
- Bij de besluitvorming over de gebundelde en in samenhang gepresenteerde wijzigingsverzoeken.

### Op welke manier

- Deelname in een werkgroep, georganiseerd door de beheerorganisatie in aanloop naar een nieuwe release van de afsprakenset. In deze werkgroep worden alle wijzigingsverzoeken in samenhang besproken. De werkgroep speelt een belangrijke rol in het doorlopen van het Matchingsproces en toepassing van het Afwegingskader uitbreiding modelgegevensset. 
- Voor wijzigingsverzoeken die niet zijn gericht op de modelgegevensset wordt gezamenlijk bepaald hoe en met wie deze afgestemd worden. Voorbeeld: een wijzigingsverzoek op het juridisch kader of voorgestelde maatregelen op het vlak van privacy kan belegd worden in een ad hoc georganiseerde juridische werkgroep. De beheerorganisatie stemt met ActiZ af voor welke verzoeken inbreng van zorgaanbieders (en/of andere branches) nodig is en op welke manier dit georganiseerd moet worden (en met wie). Dit kan ook in de werkgroep zelf zijn.
- In samenspraak met ActiZ wordt een afspiegeling van zorgaanbieders (en/of andere zorgbranches) gekozen in de functionele werkgroep/ referentiegroep. Het gaat dan onder andere om een afspiegeling van informatiseringsgraad, leervermogen, verschillende samenstelling in gebruikte softwarepakketten, verschillende samenstelling in grootte, verschillende samenstelling in opvatting over kwaliteit en bedrijfsvoering. Het aantal zorgaanbieders moet in balans zijn met (het aantal) overige deelnemers van de werkgroep/ referentiegroep. Aandachtspunt hierbij is: willen aanbieders vanuit deze verschillende doelgroepen hier tijd aan besteden?
- ActiZ kan zelf ook deelnemer zijn aan de functionele werkgroep zodat duidelijk is wat eventuele discussiepunten in de werkgroep zijn, dan wel achtergrond bij adviezen van de werkgroep. Is dat niet het geval, dan wordt ActiZ, als deelnemer KIK-V, geïnformeerd door de beheerorganisatie over de input van zorgaanbieders. ActiZ kan er voor kiezen verdere consultatie bij de achterban (en/of andere brancheorganisaties) te doen op onderwerpen en de uitkomsten daarvan in te brengen in de werkgroep. Op die manier kan eventueel een breder gedragen advies uitgebracht worden ten aanzien van de voorgestelde wijzigingen die in voorstellen ter besluitvorming worden voorgelegd aan de Ketenraad.
- ActiZ kan ervoor kiezen voorafgaand aan de besluitvorming in de Ketenraad nog een bredere consultatie te doen bij de achterban (en/of andere brancheorganisaties) ten aanzien van de voorstellen. Zij is op de hoogte van de achtergrond bij de voorstellen via de deelname aan de werkgroep.

## Ontwikkeling uitwisselprofiel

### Doel van betrokkenheid aanbieders

- Toets op achtergrond en onderbouwing betreffende uitvraag en bijbehorende informatievragen. Adviesrol richting de afnemer over nut- en noodzaak van informatievragen.
- Adviesrol richting afnemer en beheerorganisatie in het matchingsproces op de hierin benoemde stappen om te komen tot een gedragen uitwisselprofiel. 
- Adviesrol richting afnemer en beheerorganisatie bij de (verdere) invulling van en te maken keuzes in betreffende uitwisselprofiel (op basis van de benoemde onderwerpen in het modeluitwisselprofiel) evenals een toets op conceptvoorstellen voor dat uitwisselprofiel.
- (mede) Bepalen belang en nut/noodzaak peilmoment, peilperiode en uitvraagmoment in relatie tot de uitwisselkalender. 
- Toets op algemene praktische uitvoerbaarheid en implementeerbaarheid van de voorgenomen afspraken inclusief de benodigde doorlooptijd voor implementatie.
- Advies beheerorganisatie op logische samenhang tussen de bestaande uitwisselprofielen

### In welke stappen

- In het matchingsproces, onder andere:
  -Aangeven welke vergelijkbare informatievragen en/of concepten in andere uitvragen worden toegepast;
  - Nadere uitwerking van concepten en advies inzake bestaande bronnen voor die concepten in de informatievraag;
  - Toets op beschikbaarheid van gegevens in de operationele processen en systemen (zie ook ontwikkeling afsprakenset);
  - Aangeven of gehanteerde peilperiodes, peilmomenten en momenten van uitvraag kunnen aansluiten bij bestaande momenten in andere uitvragen, dan wel aangeven of hergebruik van gegevenssets en antwoorden op informatievragen mogelijk is.
- Bij de inhoudelijke afstemming over het concept uitwisselprofiel, dan wel op de gebundelde en in samenhang gepresenteerde wijzigingsverzoeken op bestaande uitwisselprofielen.
- Bij de besluitvorming over een uitwisselprofiel, dan wel de besluitvorming inzake de gebundelde en in samenhang gepresenteerde wijzigingsverzoeken op bestaande uitwisselprofielen.

### Op welke manier

- Bij verzoeken tot nieuwe uitwisselprofielen start de beheerorganisatie het proces tot ontwikkeling van het uitwisselprofiel. In samenspraak met de afnemer en ActiZ wordt een groep aanbieders (en/of andere zorgbranches) gekozen die fungeert als een klankbordgroep op de voorstellen die in de verschillende stappen van het proces tot stand komen. ActiZ kan zelf ook deelnemer zijn aan de klankbordgroep. De klankbordgroep adviseert afnemer en beheerorganisatie en kent een eigen dynamiek die niet gebonden is aan het releaseproces van de afsprakenset, tenzij wijziging in afspraken (en daarbinnen de model gegevensset) nodig zijn. Dit kan dezelfde groep aanbieders zijn die deelnemen aan de hiervoor genoemde werkgroep (ontwikkeling afsprakenset), maar er kan ook gekozen worden voor een andere afspiegeling in relatie tot de kenmerken van betreffende uitvraag. 
- Voor wijzigingsverzoeken op een bestaand uitwisselprofiel beoordeelt de beheerorganisatie in samenspraak met de afnemer en ActiZ of inbreng van zorgaanbieders nodig is en op welke manier dit georganiseerd moet worden (en met wie).
- De behandeling van nieuwe uitwisselprofielen of wijzigingsverzoeken op een bestaand uitwisselprofiel kan leiden tot de volgende twee situaties: 
  - Er zijn wijzigingen op de afsprakenset nodig om de wijziging op het uitwisselprofiel of een nieuw uitwisselprofiel te kunnen uitvoeren. In de werkgroep worden de voorstellen voor wijzigingen op de bestaande afsprakenset besproken die nodig zijn voor een nieuw uitwisselprofiel of de wijziging van een bestaand uitwisselprofiel. Deze wijzigingsvoorstellen zijn onderdeel van de oplevering van een nieuwe release. De werkgroep speelt een belangrijke rol in het doorlopen van het Matchingsproces en toepassing van het Afwegingskader uitbreiding modelgegevensset. Voor verdere uitwerking van de betrokkenheid bij de werkgroep, zie de beschrijving van Ontwikkeling afsprakenset hierboven. 
  - Voorstellen voor nieuwe uitwisselprofielen die geen aanpassingen behoeven in de afsprakenset worden met een advies van de beheerorganisatie voorgelegd volgens het proces, zoals weergegeven in [Beheerafspraken - Vaststellen](../../#3-vaststellen). In het advies wordt de afstemming met de klankbordgroep weergegeven inclusief de verwerking van de punten die hieruit naar voren zijn gekomen.
