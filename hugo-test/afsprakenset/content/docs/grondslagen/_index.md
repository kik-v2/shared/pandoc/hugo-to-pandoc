---
title: "Grondslagen"
bookCollapseSection: true
weight: 5
---

# Grondslagen

De grondslagen vormen het fundament van de afsprakenset en bestaan uit:

{{<section>}}
  
{{< hint info >}}
In de grondslagen worden de navolgende begrippen gehanteerd die een belangrijke rol hebben in de gehele afsprakenset: 
{{< /hint >}}

| Begrip | Omschrijving | Synoniemen |
|:-|:-|:-|
| Aanbiedergegevensset | De Aanbiedergegevensset is een ten behoeve van KIk-V door de Aanbieder beschikbaar gestelde gegevensset conform de Modelgegevensset | Aanbiedergegevensset KIK-V |
| Aanbieder(s) | Aanbieders nemen deel aan de afsprakenset. Zij valideren en beantwoorden informatievragen aan Afnemers volgens de in de afsprakenset gedefinieerde spelregels. Voor de eerste versie van de afsprakenset zijn de Aanbieders  instellingen voor verpleeghuiszorg. | |
| Afnemer(s) | Afnemers nemen deel aan de afsprakenset. Zij formuleren en stellen informatievragen aan Aanbieders volgens de in de afsprakenset gedefinieerde spelregels. `Voor de eerste versie van de Afsprakenset betreft dit de partijen die als afnemer betrokken zijn bij het programma KIK-V.` | |
| Afsprakenset | Set van afspraken op juridisch, organisatorisch, financieel, semantisch en technisch gebied om alle partijen voldoende vertrouwen te geven in hetgeen het stelsel hen biedt. Partijen die deelnemen aan de Afsprakenset KIK-V committeren zich aan de afspraken, en kunnen op basis van de reeds overeengekomen afspraken gegevens uitwisselen. |Afsprakenset KIK-V, Afsprakenstelsel |
| Beheerafspraken | De beheerafspraken zijn een verzameling verantwoordelijkheden voor de uitvoering van de beheeractiviteiten. | | 
| Beheerorganisatie | De Beheerorganisatie is verantwoordelijk voor het beheer van de Afsprakenset KIK-V. De term omvat zowel de besluitvorming als de uitvoering. | |
| Deelnemers | Afnemers en Aanbieders vormen tezamen de Deelnemers. | | 
| KIK-V | (Een programma voor) duurzame afspraken over informatievoorziening voor de kwaliteitsregistraties in de verpleeghuiszorg. KIK-V staat voor Keteninformatie Kwaliteit Verpleeghuiszorg. | |
| Kwaliteitskader Verpleeghuiszorg | Het Kwaliteitskader Verpleeghuiszorg beschrijft wat cliënten en hun naasten mogen verwachten van verpleeghuiszorg. Daarnaast biedt dit document opdrachten voor zorgverleners en zorgorganisaties om samen de kwaliteit te verbeteren en het lerend vermogen te versterken. Het vormt het kader voor extern toezicht en voor inkoop en contracteren van zorg. | |
| Modelgegevensset | De modelgegevensset is een verzameling definities van gegevenselementen die worden gebruikt in de uitwisseling van informatie over de kwaliteit van verpleeghuiszorg. | Modelgegevensset KIK-V |
| Uitwisselprofielen | Een uitwisselprofiel is een verzameling verantwoordelijkheden van de rollen afnemer en aanbieder, die gelden voor een set van een of meer vragen en antwoorden en moeten worden toegepast in de operationele gegevensuitwisseling. Het profiel kan betrekking hebben op een of meer lagen en aspecten uit het interoperabiliteitsmodel. | |
