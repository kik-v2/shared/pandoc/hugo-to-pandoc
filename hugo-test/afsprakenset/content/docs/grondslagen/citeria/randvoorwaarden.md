---
title: "Randvoorwaarden"
description: "Specifiek voor het ontwerpproces van de afspraken gelden nog de volgende randvoorwaarden"
weight: 20
---

# Randvoorwaarden

{{< hint info >}}
{{< param description >}}
{{< /hint >}}

| Nr. | Beschrijving |
|:-|:-|
| R1 | De uiteindelijke afsprakenset zal op elk moment in lijn moeten zijn met de Nederlandse wet- en regelgeving. Daarom moet de afsprakenset zo zijn opgezet dat partijen die betrokken zijn bij de uitvoering ervan in staat worden gesteld te voldoen aan deze wet- en regelgeving. Dit betekent vooral dat een goede uitvoering van de afspraken niet mag vereisen dat partijen afwijken van wet- en regelgeving. Belangrijke juridische kaders, zoals wetgeving ten aanzien van kwaliteit van de zorg en de AVG, zullen voor dat doel getoetst en toegelicht worden. |
| R2 | In het ontwerpproces is het van belang om inzicht te hebben in de informatiseringsgraad en de inrichting van interne werkprocessen (rondom kwaliteit, bedrijfsvoering etc.) bij ketenpartijen. Het succes van de afsprakenset wordt bepaald door de mate waarin het ontwerp aansluit bij de huidige praktijk en de mogelijkheid biedt om stapsgewijs te implementeren, bijvoorbeeld door steeds kleine aanpassingen te doen. Dit uiteraard in combinatie met het leer- en verbetervermogen van de ketenpartijen. |
| R3| Belangrijke randvoorwaarde voor een werkende oplossing is dat tijdig duidelijkheid bestaat over de beheersituatie voor de lange termijn. Voor het vertrouwen tussen de ketenpartijen en in een stabiele uitgangssituatie bij implementatie moet duidelijk zijn dat de oplossing voor langere termijn is geborgd en wordt bestuurd. Eventueel beheer moet duurzaam buiten het tijdelijke programma KIK-V kunnen worden belegd. |
| R4 | In het ontwerpproces zal de samenhang met andere lopende ontwikkelingen in de verpleeghuiszorg bewaakt worden. In het maken van de afspraken en het ontwerpen van de governancestructuur wordt per definitie afstemming gezocht met andere programma’s in de langdurige zorg of in het kader van een duurzaam informatiestelsel zorg. |
| R5 | Transparante en open besluitvorming is een randvoorwaarde voor succes. Voor zowel gebruikers van de gegevens, bronhouders als overige belanghebbenden geldt dat het vertrouwen in de afsprakenset wordt verhoogd als de voortgang van de ontwikkeling ervan inzichtelijk is, en helder is hoe belangrijke afwegingen zijn gemaakt. Hiervoor sluit het ontwerpproces aan bij de vergadercyclus van het programma KIK-V. |
| R6 | Snelheid van oplevering van een eerste conceptversie van een afsprakenset is van belang. Het momentum op dit onderwerp is nu, onder andere door de huidige investeringen in kwaliteit van de verpleeghuiszorg. De eerste conceptversie kan vanaf april 2020 worden gebruikt voor het gesprek hierover met de ketenpartijen, de volledige oplossing april 2021. |
| R7 | De hoeveelheid tijd die door zorgprofessionals aan registratie ten behoeve van kwaliteitsinformatie wordt besteed mag niet stijgen door de afsprakenset. |
| R8 | Voor deelnemers in de afsprakenset geldt dat zij hun wettelijke taak moeten kunnen blijven uitvoeren. Ook over situaties waarin het KIK-V proces door omstandigheden niet als vastgesteld kan worden doorlopen, worden onderlinge afspraken vastgelegd. Deze afspraken zijn onderdeel van de afsprakenset KIK-V en conform de principes KIK-V. |
