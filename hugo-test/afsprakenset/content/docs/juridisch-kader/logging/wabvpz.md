---
title: "WABVPZ"
bookToc: false
description: "Wet aanvullende bepalingen verwerking persoonsgegevens in de zorg"
weight: 10
---

# **[{{< param description >}}](https://wetten.overheid.nl/BWBR0023864/2020-07-01)**

Geldend van 01-07-2020 t/m heden

{{< columns >}}

**Relevante artikelen**

*Artikel 15j*
  
1. Bij algemene maatregel van bestuur kunnen regels worden gesteld over de functionele, technische en organisatorische maatregelen voor het beheer, de beveiliging en het gebruik van een zorginformatiesysteem of een elektronisch uitwisselingssysteem.
2. De voordracht voor een algemene maatregel van bestuur als bedoeld in het eerste lid, wordt niet eerder gedaan dan vier weken nadat het ontwerp aan beide kamers der Staten-Generaal is overgelegd. Indien een der kamers der Staten-Generaal besluit niet in te stemmen met het ontwerp, wordt er geen voordracht gedaan en kan niet eerder dan zes weken na het besluit van die kamer der Staten-Generaal een nieuw ontwerp aan beide kamers der Staten-Generaal worden overgelegd.

<--->

**Toepassing voor de afspraken**

De *wet aanvullende bepalingen verwerking persoonsgegevens in de zorg* biedt de basis voor logging van de verwerkingen in het kader van KIK-V door aanbieders. Onder art. 15j van de Wet aanvullende bepalingen verwerking persoonsgegevens in de zorg kunnen namelijk nadere regels gesteld worden ten aanzien van zorginformatiesystemen of elektronische uitwisselingssystemen, waaronder op het gebied van logging. Deze zijn nader ingevuld met het besluit elektronische gegevensverwerking door zorgaanbieders.

{{< /columns >}}