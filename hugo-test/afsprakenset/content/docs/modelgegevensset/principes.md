---
title: "Principes"
description: "Principes KIK-V modelgegevensset"
weight: 10
---

# {{< param description >}}

### Principe 1
De modelgegevensset heeft als doel om aanbieders duidelijkheid te verschaffen welke gegevens zij moeten ontsluiten ten behoeve van de kwaliteitsmetingen.

### Principe 2
De modelgegevensset is een volledige, uitsluitende set van gegevenselementen over de kwaliteit van verpleeghuiszorg.

### Principe 3
De modelgegevensset bevat enkel de gegevenselementen die gebruikt worden in een genormaliseerde informatievraag. 

### Principe 4
De modelgegevensset sluit aan op bestaande (uitwissel-)standaarden (bijvoorbeeld zorginformatiebouwstenen) indien beschikbaar (en zeker indien er een wettelijke verplichting geldt), maar is een onafhankelijke set. Dit houdt in dat veranderingen in externe standaarden pas in de modelgegevensset doorgevoerd worden wanneer deze getoetst zijn binnen de doelen van de modelgegevensset en dat externe veranderingen door de beheerorganisatie moeten worden gemonitord en afgestemd.

### Principe 5
Definities worden zo veel mogelijk op breed toepasbare, herbruikbare wijze gedefinieerd. Indien een definitie hierdoor niet specifiek genoeg is, kan gekozen worden om het concept in meerdere (sub)concepten uit te splitsen.

### Pincipe 6
Indien beschikbaar en mogelijk wordt aangesloten bij reeds bestaande definities. De bron voor deze definities dient breed gedragen, maar specifiek genoeg te zijn. Bijvoorbeeld de CAO.

### Principe 7
De modelgegevensset is onafhankelijk van de stand der techniek: de opgenomen elementen hebben geen relatie met de technische mogelijkheden voor extractie en/of uitwisseling.

### Principe 8
Aanbieders kunnen zelf bepalen hoe zij de gevraagde elementen uit bronsystemen halen.
