---
title: "Modeluitwisselprofiel"
bookCollapseSection: true
weight: 6
---

# Modeluitwisselprofiel KIK-V

{{< hint info >}}
Het modeluitwisselprofiel beschrijft de [principes](principes) voor uitwisselprofielen en de [onderdelen](onderdelen) waar ze uit bestaan.
{{< /hint >}}

![KIK-V Matching proces 6](kik-v-matching-proces-6.png)
