---
title: "Onderdelen"
weight: 20
---

# Onderdelen
In een uitwisselprofiel wordt opgenomen:

## Titel
- Uit de titel van het uitwisselprofiel wordt duidelijk welke afnemer voor welk doel een uitwisseling afspreekt.
- De titel van het uitwisselprofiel begint met de naam van de afnemer, zodat in het overzicht van uitwisselprofielen snel onderscheidenlijk is voor welke uitwisseling die bedoeld is. 

## Algemeen

- Metadata conform de tabel Metadata uitwisselprofiel.
- Doel van de uitvraag en wat er met de gevraagde informatie wordt gedaan.
- Per informatievraag doel en rationale van de vraag.

## Juridische interoperabiliteit

- Wat de juridische grondslag van de afnemer voor de gegevensverwerking is. Er kan sprake zijn van een wettelijke grondslag (zie voor overzicht van mogelijke wettelijke grondslagen het Juridisch kader) of van andere overeengekomen juridische (standaard)afspraken tussen afnemer en aanbieders.
- Wat het doel van de gegevensverwerking is. Op basis van het doel moet worden bepaald welke gegevens noodzakelijk en proportioneel zijn om het doel te bereiken.
- Waar de verantwoordelijkheid ligt voor (de controle van) de uitkomsten van de door de afnemer toegepaste analyse op de gegevens.

## Organisatorische interoperabiliteit

- Wat de (gewenste) momenten van gegevensuitwisseling zijn, zowel aanlevering als terugkoppeling. Deze momenten dienen aan te sluiten bij de Uitwisselkalender.
- Wat de (gewenste) peildata of peilperiode voor de gegevens zijn. Deze momenten dienen aan te sluiten bij de Uitwisselkalender.
- Wat de (gewenste) bewaartermijn is van de aanbiedergegevensset die door de aanbieder wordt gebruikt voor de beschreven gegevensuitwisseling. 
- Welke afspraken gelden bij twijfels/vragen over de kwaliteit van data.
- Welke in- en exclusiecriteria gelden voor het bepalen van de doelgroep van de uitwisseling. Hieronder vallen ook eventuele drempelwaarden om te voorkomen dat geanonimiseerde gegevenssets gedeanonimiseerd kunnen worden.
- Welke in- en exclusiecriteria gelden voor de toerekening van een locatie (van een zorgaanbieder) tot een locatie met Wlz-zorg (en meer specifiek verpleeghuiszorg).

## Semantische interoperabiliteit

- Welke informatievragen de afnemer wil beantwoorden en welke definities daarbij gehanteerd worden.

Per informatievraag het doel en de achtergrond van de informatievraag.
- Welke aanvullende in- en exclusiecriteria gelden voor de informatievragen.
- Welke gegevenselementen nodig zijn om de informatievragen te beantwoorden. De benodigde gegevenselementen moeten onderdeel zijn van de Modelgegevensset. 
- Welke eisen worden gesteld aan de actualiteit, betrouwbaarheid en volledigheid van de data. 
- Welke berekening/rekenregels van toepassing op de gegevens uit de model gegevensset KIK-V en waar deze berekening/rekenregels is gepubliceerd.
- Het aggregatieniveau waarop de dataset wordt aangeleverd en de aggregatieslag die de aanbieder daarbij moet maken.
- Welke contextinformatie meegegeven kan worden vanuit welke bestaande kwalitatieve bron(nen).

## Technische interoperabiliteit

- Welke manier(en) van gegevensuitwisseling wordt/worden gehanteerd voor de aanlevering door aanbieders.
- Welke manier(en) van gegevensuitwisseling wordt/worden gehanteerd voor de terugkoppeling door de afnemer.

`Afspraken over de werking van de voorzieningen zijn over het algemeen onderdeel van de documentatie van de gebruikte voorzieningen voor gegevensuitwisseling zelf. Het uitwisselprofiel verwijst naar de relevante documentatie.`

## Privacy- en informatiebeveiliging

- Indien voor de aggregatie bij aanbieders gezondheidsgegevens moeten worden verwerkt, welke toetsing heeft plaatsgevonden om vast te stellen of de verwerking noodzakelijk en proportioneel is omwille van de kwaliteitsborging van zorg. 

Enkel in dat geval zijn de uitzonderingsgronden in AVG artikel 9, lid 1, onder h en i van toepassing (zie Juridisch kader).
- Welke toetsing heeft plaatsgevonden om vast te stellen of met de voorgestelde aggregatie door aanbieders tot onomkeerbare anonimisering kan worden gekomen (zie ook Toetsing privacy- en informatiebeveiliging).
- Welke maatregelen nodig zijn om privacy en informatiebeveiliging te borgen bij de gegevensuitwisseling. Dit is mede afhankelijk van de mate van privacy- en concurrentiegevoeligheid van gegevens. Hieronder vallen ook maatregelen die nodig zijn om te komen tot onomkeerbare anonimisering.
- Welke aanvullende maatregelen nodig zijn om concurrentiegevoelige gegevens te beschermen.
