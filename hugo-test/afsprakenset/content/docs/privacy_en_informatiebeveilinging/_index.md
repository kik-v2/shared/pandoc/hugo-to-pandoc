---
title: "Privacy en informatiebeveiliging"
weight: 4
---

# Privacy en informatiebeveiliging deelnemers

Afnemers en aanbieders zijn zelf verantwoordelijk voor een gedegen privacy- en informatiebeveiligingsbeleid en dienen voor de eigen organisatie de noodzakelijke maatregelen te treffen om privacy en informatiebeveiliging te borgen. De volgende handleidingen kunnen daarbij handvatten bieden:

- [Handleiding Algemene verordening gegevensbescherming (AVG)](https://www.rijksoverheid.nl/documenten/rapporten/2018/01/22/handleiding-algemene-verordening-gegevensbescherming). Uitgegeven door het Ministerie van Justitie en Veiligheid.
- [Guidelines on Data Protection Impact Assessment (DPIA) (wp248rev.01)](https://ec.europa.eu/newsroom/article29/item-detail.cfm?item_id=611236). Van de voorganger van de EDPB, waarvan ook een Nederlandstalige versie beschikbaar is.

Daarnaast toetst de beheerorganisatie doorlopend de privacy en informatiebeveiliging van de uitwisselprofielen en de bredere afsprakenset (zie hiervoor [Toetsing privacy- en informatiebeveiliging](https://zorginstituutnl.atlassian.net/wiki/spaces/KIKVOP/pages/2066481189/Toetsing+privacy-+en+informatiebeveiliging)).

## Logging

Van afnemers en aanbieders wordt verwacht dat zij verwerkingen in het kader van KIK-V loggen. Zo kan tijdig worden ingegrepen bij onrechtmatige verwerkingen, hetzij als gevolg van (on)opzettelijk foutief handelen, kwaadwillende derden of beheersfouten.
  

Voor aanbieders volgt de grondslag om te loggen uit een combinatie van de [Algemene Verordening Gegevensbescherming](../juridisch-kader/algemeen/avg), de [Wet aanvullende bepalingen verwerking persoonsgegevens in de zorg](../juridisch-kader/logging/wabvpz) en het [Besluit elektronische gegevensverwerking door zorgaanbieders](../juridisch-kader/logging/begdza). In deze wet- en regelgeving is tevens vastgelegd dat logging moet plaatsvinden volgens de [NEN-7513](https://www.nen.nl/nen-7513-2018-nl-245399) norm.
  

Voor afnemers bestaan verschillende grondslagen voor logging, afhankelijk per afnemer en situatie. De afnemer dient in het kader van de eigen bedrijfsvoering te bepalen op welke wijze logging wordt toegepast, wat hiervoor de grondslag is en in hoeverre daarbij persoonsgegevens van medewerkers worden verwerkt (zie [Juridisch kader / Categorie logging / Afnemers](../juridisch-kader/logging/afnemers))