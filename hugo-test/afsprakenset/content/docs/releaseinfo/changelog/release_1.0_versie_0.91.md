---
title: "Release 1.0 versie 0.92"
weight: 30
---

# Changelog release 1.0 versie 0.91

- Wijzigingen naar aanleiding van laatste input uit Tactisch Overleg december 2020
- Aanpassing figuren [opbouw afsprakenset](../../../opbouw) (uitwisselcontexten als apart onderwerp)
- Aanpassing [betrokkenheid aanbieders bij ontwikkeling](../../../beheerafspraken/ontwikkeling/betrokkenheid_aanbieders)
- Aanpassing titel pagina Procesafspraken stuurgroep kwaliteitskader voor uitwisselprofiel ODB met ‘voor uitwisselprofiel ODB’