---
title: "Verantwoordelijkheden"
weight: 10
---

# Gedetailleerde verantwoordelijkheidsverdeling sturingsstructuur

## Ketenraad KIK-V (strategische-besluitvormingsfunctie)

- Vaststelling van de strategische doorontwikkelagenda;
- Vaststellen van majeure releases van de afsprakenset, inclusief de bijbehorende implementatieafspraken;
- Vaststellen van uitwisselprofielen die aanpassing van de afsprakenset vergen;
- Vaststellen en ondertekenen van nieuwe versies en/of addenda op het Convenant KIK-V;
- Vaststellen van de opdracht van de beheerorganisatie (totstandkoming loopt in afstemming met het ministerie van VWS en de beheerorganisatie);
- Vaststellen van een gezamenlijk communicatieplan (zie Convenant KIK-V);
- Inhoudelijk aansturen van de beheerorganisatie;
- Bewaken van onderlinge samenhang met andere initiatieven en het organiseren van samenhang in de sturing, waaronder ook bewaken van de relatie met het informatieberaad en softwareleveranciers;
- Monitoren, agenderen en behandelen van belangrijke ontwikkelingen voor KIK-V;
- Behandelen van verzoeken om af te wijken van de afsprakenset (procedure afwijkende uitvraag);
- Overleg en/of besluitvorming bij niet-naleving;
- Oplossen van onderlinge geschillen over de afsprakenset KIK-V tussen aanbieders en afnemers;
- Zich laten informeren over de besluitvorming van het Tactisch overleg KIK-V;
- Toezien op de werking van de afsprakenset (en het proces van besluitvorming) en het periodiek evalueren.

## Tactisch overleg KIK-V (tactische-besluitvormingsfunctie)

- Integraal afstemmen over plannen, op te leveren producten en resultaten;
- Voorbereiden (strategische) besluitvorming Ketenraad KIK-V;
- Vaststellen van mineure wijzigingen aan de afsprakenset en uitwisselprofielen (wijzigingen waarbij een integrale bestuurlijke afweging niet loont);
- Vaststellen van urgente wijzigingen aan de afsprakenset en uitwisselprofielen (wijzigingen waarbij een integrale bestuurlijke afweging te veel tijd kost);
- Vaststellen van uitwisselprofielen die volledig passen binnen de kaders van de afsprakenset;
- Informeren van de Ketenraad KIK-V over tactische besluitvorming.

## Beheerorganisatie (uitvoeringsfunctie)

- Faciliteren van de (door)ontwikkeling van de strategische ontwikkelagenda;
- Faciliteren van de (door)ontwikkeling van de afsprakenset en de uitwisselprofielen:
  - Het bepalen van de impact van ontwikkelingen uit de omgeving op de afsprakenset;
  - Het registreren, prioriteren en afhandelen van werkzaamheden die voortkomen uit deze ontwikkelingen;
  - Het onderhouden van bestaande functionaliteit en het ontwikkelen van nieuwe functionaliteit. Onderdeel hiervan is de uitvoering van het matchingsproces waarmee voor de modelgegevensset vraag en aanbod bij elkaar worden gebracht;
  - Faciliteren van de totstandkoming van wijzigingsvoorstellen;
  - Het toetsen van voorstellen bij vertegenwoordigers van belanghebbende partijen (o.a. via werkgroepen) t.b.v. de kwaliteit, het draagvlak en efficiënte besluitvorming;
  - Het (laten) toetsen van nieuwe functionaliteit in de praktijk;
  - Het uitvoeren van stelsel-brede onderzoeken op het gebied van o.a. privacy en informatiebeveiliging;
  - Het borgen van de relatie tussen de afsprakenset, de uitwisselprofielen en het Convenant KIK-V;
  - Het periodiek evalueren van het (reguliere) beheerprocessen en de procedure voor afwijkende uitvragen.
- Samenstellen van releases van de afsprakenset en uitwisselprofielen en het faciliteren van besluitvorming bij de Ketenraad KIK-V en/of het Tactisch overleg KIK-V;
- Voeren van het secretariaat van de Ketenraad KIK-V en het Tactisch overleg KIK-V;
- Faciliteren van de vormgeving, publicatie en verspreiding van de afsprakenset en uitwisselprofielen, alsmede de registratie en verslaglegging van activiteiten om hiertoe te komen;
- Het inrichten en onderhouden van systemen ten behoeve van de publicatie van de afsprakenset en uitwisselprofielen en ter ondersteuning van de operationele gegevensuitwisseling (denk bijvoorbeeld aan een gezamenlijke centrale voorziening of een validatiemodule voor de aanbiedergegevensset);
- Faciliteren van de vormgeving, publicatie en verspreiding van een gezamenlijke communicatieplan, alsmede de registratie en verslaglegging van activiteiten om hiertoe te komen.
- Faciliteren van algemene communicatie over de ontwikkeling en inhoud van de afsprakenset en wijzigingscommunicatie;
- Faciliteren van een helpdesk voor het beantwoorden van (toepassings)vragen;
- Vormgeven van het relatiebeheer met:
  - Softwareleveranciers;
  - Andere beheerorganisaties;
  - Flankerende organisaties en ontwikkelingen.

{{< hint warning >}}

Een belangrijk aandachtspunt bij de invulling van deze taak is het grote aantal initiatieven dat op afnemers, aanbieders en hun softwareleveranciers afkomt. Een samenhangende benadering van initiatieven is dan ook nodig. Van de beheerorganisatie wordt verwacht dat zij waar mogelijk en relevant afstemming zoekt met deze andere initiatieven om te komen tot een gezamenlijke benadering van softwareleveranciers, afgestemde en overzichtelijke releasekalenders en implementatiebenaderingen en een eenduidige toepassing van (bestaande) standaarden. In praktijk betekent dit onder andere afstemming met partijen zoals:

- Nictiz (over de zorginformatiebouwstenen);
- de Stuurgroep Kwaliteitskader Verpleeghuiszorg (over de kwaliteitsstandaard en –indicatoren verpleeghuiszorg);
- Verenso en V&VN (over de informatiestandaarden Verpleeghuiszorg);
- Het ministerie van VWS (over het waarborgen van de samenhang in het zorginformatiestelsel);
- Het Informatieberaad (over het waarborgen van de samenhang in het zorginformatiestelsel).

{{< /hint >}}

- Houden van toezicht op de naleving van de afspraken onder aanbieders en afnemers en het, indien nodig, adresseren van non-conformiteiten bij de Ketenraad KIK-V. De beheerorganisatie geeft verschillende vormen van toezicht vorm:
- Het actief monitoren van het gebruik van de afsprakenset (operationeel toezicht);
- Het doen van gericht onderzoek naar specifieke thema’s in het gebruik (thematisch toezicht);
- Het doen van onderzoek naar aanleiding van klachten (reactief toezicht).
- Bieden van inhoudelijke expertise bij geschillen over de afsprakenset en/of uitwisselprofielen tussen aanbieders en afnemers en het, indien nodig, escaleren naar de Ketenraad KIK-V;
- Afgeleide taken voor het functioneren van de uitvoeringsfunctie zelf (financiële administratie, planning & control taken, management, etc.), met inbegrip van managementinformatie en verantwoording. Evenals het vastleggen, documenteren en archiveren van de ontwikkel- en afstemprocessen, bijvoorbeeld middels verslagen van bijeenkomsten, afstemming met omgeving, e.a.
