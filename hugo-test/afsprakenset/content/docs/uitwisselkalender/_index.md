---
title: "Uitwisselkalender"
bookCollapseSection: true
weight: 8
---

# Uitwisselkalender

De uitwisselkalender beschrijft in samenhang de aanlever- en terugkoppelmomenten binnen KIK-V en is daarmee een instrument dat inzicht geeft in de timing van de gegevensuitwisseling. Ook de peilmomenten of verslagperiode van de gegevens spelen daarbij een rol. Dit inzicht is relevant voor zowel aanbieders (wanneer moeten bepaalde gegevens klaarstaan) als afnemers (wanneer zijn bepaalde gegevens beschikbaar).

{{< hint info >}}
Voor nieuwe informatiebehoeften van deelnemers in de afsprakenset functioneert de kalender als referentiekader. Er vindt sturing plaats op het aansluiten bij een bestaand moment van uitvraag en peilmomenten of verslagperiode. Na vaststelling van een (wijziging op een) uitwisselprofiel wordt het aanlever- en/of peilmoment toegevoegd aan de uitwisselkalender. 
{{< /hint >}}
  
![Uitwisselmomenten niet afgestemd](uitwisselmomenten-niet-afgestemd.png)

![Het effect van de uitwisselkalender](het-effect-van-de-uitwisselkalender.png)

{{< hint warning >}}
**Aandachtspunt bij release 1.0 versie 0.9**
  

Er is voor de 0.9-versie van de afsprakenset gekozen om de huidige aanlever- en peilmomenten te tonen voor de gekozen afbakening (uitvragen personeelssamenstelling en basisveiligheid). Het biedt daarmee inzicht in kansen voor vereenvoudiging in het aantal aanlever- en/of peilmomenten (of verslagperiodes), de timing van de aanlevermomenten evenals inzicht in mogelijk hergebruik van gegevens uit eerdere aanleveringen op een later aanlevermoment. 
{{< /hint >}}

De aanbieders leveren op dit moment aan ketenpartijen KIK-V drie soorten gegevenssets aan die (gedeeltelijk) betrekking hebben op kwaliteit in de verpleeghuiszorg. Het betreft aanleveringen aan de volgende partijen, die aanbieders chronologisch in de volgende volgorde aanleveren:

1. Aan de zorgkantoren over de verantwoording van het kwaliteitsbudget: cumulatief over het eerste, tweede en derde kwartaal en over het hele jaar (aanleverdata respectievelijk 1 juni T, 1 augustus T, 1 november T en 1 april T+1). 
2. Daarnaast gegevens over het hele jaar in een zogenoemd rondrekenmodel als onderdeel van het accountantsprotocol kwaliteitsbudget (aanleverdatum 1 april T+1).
3. Aan Zorginstituut Nederland, kwaliteitsgegevens verpleeghuiszorg bestaande uit indicatoren kwaliteit en indicatoren personeelssamenstelling, over het hele jaar (aanleverdatum 1 juli T+1).
  

Daarnaast leveren aanbieders ook aan voor het volgende:

- Aan het CIBG, het onderdeel personeel in de Jaarverantwoording Zorg (DigiMV), over het hele jaar (aanleverdatum 1 juni T+1).