---
title: "Criteria"
description: "Criteria geven aan langs welke meetlat het succes van het afsprakenstelsel kan worden afgemeten. Criteria bestaan uit doelen (factoren waarbij gestreefd wordt naar een zo hoog mogelijke score, waarbij afwegingen tussen de doelen kunnen bestaan) en randvoorwaarden (niet-onderhandelbare eisen). De totstandkoming van het stelsel (het ontwerp- en beheerproces) en de inhoud van de afspraken zijn verweven; doelen kunnen dan ook betrekking hebben op beide aspecten. De nummering impliceert geen prioritering."
bookCollapseSection: true
weight: 3
---

# Citeria

{{< hint info >}}
{{< param description >}}
{{< /hint >}}

{{<section>}}