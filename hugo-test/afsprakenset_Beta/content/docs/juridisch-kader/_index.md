---
title: "Juridisch kader"
bookCollapseSection: true
weight: 3
---

# Juridisch kader

{{< hint info >}}
Het juridisch kader beschrijft relevante wet- en regelgeving voor de afsprakenset KIK-V en brengt de belangrijkste kaders voor de oplossing in beeld.

- De wet- en regelgeving is geordend in **Categorieën** die overeenkomen met de verwerkingen binnen KIK-V. Voor de wet- en regelgeving die niet specifiek betrekking heeft op een van de verwerkingen is een **Categorie algemeen** toegevoegd.
- Onder de Categorieën staan in de eerste kolom de **Relevante artikelen** uit wet- en regelgeving weergegeven. In de tweede kolom **Toepassing voor de afspraken** is vervolgens opgenomen hoe de artikelen worden geïnterpreteerd en op welke manier ze van toepassing zijn voor de oplossing.. 

Rondom specifieke doelen of wettelijke taken van partijen kunnen aanvullende regels zijn over welke (persoons)gegevens verwerkt mogen worden. Het voert te ver om dit voor elk doel of elk soort gegevens toe te lichten in het kader. Op het moment dat de gegevensuitwisseling wordt uitgewerkt in een *uitwisselprofiel*, kan worden nagegaan of er nadere restricties/inkadering zijn voor het uitwisselen van gegevens.
Het juridisch kader pretendeert niet volledig te zijn en het blijft de verantwoordelijkheid van deelnemende partijen om te voldoen aan wet- en regelgeving. Er kunnen daarmee geen rechten aan het juridisch kader worden ontleend.
{{< /hint >}}

{{<sectiontreelist>}}

