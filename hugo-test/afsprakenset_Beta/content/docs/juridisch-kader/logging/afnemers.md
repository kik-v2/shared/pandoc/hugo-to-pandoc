---
title: "Afnemers"
bookToc: false
description: "Logging door afnemers"
weight: 30
---

# **{{< param description >}}**


{{< columns >}}

**Relevante artikelen**

De grondslag voor de verwerking van persoonsgegevens bij logging is niet per definitie dezelfde per afnemer. Deze grondslagen zijn niet opgenomen in het juridisch kader. Een van de mogelijke grondslagen voor logging (en een zeer relevante) is die van het gerechtvaardigd belang van de verwerkingsverantwoordelijke of een derde (art. 6 lid 1 sub f AVG). Het bijhouden van logging (in het kader van KIK-V) valt bij afnemers namelijk niet noodzakelijkerwijs onder hun taak van algemeen belang, waardoor gebruik zou kunnen worden gemaakt van een gerechtvaardigd belang als verwerkingsgrond, in het kader van de bedrijfsvoering. De inperking van de laatste volzin van art. 6 lid 1 AVG is dan niet van toepassing. Voor het gebruik van deze grondslag dient wel een situatiespecifieke afweging gemaakt te worden tussen het belang van de verwerkingsverantwoordelijke bij de logging en het (mogelijk tegenovergestelde) belang van de betrokken medewerkers van de verwerkingsverantwoordelijke.

<--->

**Toepassing voor de afspraken**

Afnemers moeten hun *rol als afnemer binnen KIK-V* en de verwerkingen die zij hiervoor doen in het kader van de logging, meenemen in hun eigen privacy en informatiebeveiligingsbeleid.

{{< /columns >}}