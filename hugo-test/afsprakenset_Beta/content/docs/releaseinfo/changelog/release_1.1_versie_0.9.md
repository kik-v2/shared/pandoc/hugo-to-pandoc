---
title: "Release 1.1 versie 0.9"
weight: 10
---

# Changelog release 1.1 versie 0.9

Juridisch kader:

- De artikelen 66c en 66d van de Zorgverzekeringswet vervallen per 1 juli 2021 bij de invoering van de wijziging in de Wkkgz en worden vervangen door de artikelen 11i en 11g van de Wkkgz. Inhoudelijk verandert hier niet veel aan, maar de artikelen zijn met ingang van 1 juli 2021 in een andere wet opgenomen.

Model gegevensset:

- Tekstuele aanpassingen die ervoor zorgen dat de tekst beter aansluit bij de vorm van de model gegevensset, namelijk een ontologie
- Aanpassingen van de gebruikte voorbeelden voor de werking van de modelgegevensset naar actuele voorbeelden met behulp van de ontologie
- Belangrijkste wijzigingen in de ontologie ten behoeve van de ondersteuning van het uitwisselprofiel ODB kwaliteitskader zijn [hier](https://github.com/kik-v/use-cases/blob/main/Changelog.md) te vinden.

Model uitwisselprofiel:

- Aan organisatorische interoperabiliteit toegevoegd het onderwerp looptijd van een uitwisselprofiel.

Matchingsproces:

- Pagina bestaande bronnen is gewijzigd: een van de tussenkoppen is gewijzigd van ‘Nieuwe (openbare) bronnen’ naar ‘Toetsingscriteria (openbare) bronnen)’.

{{< hint info >}}
Met de publicatie van deze versie van de afsprakenset zijn ook wijzigingen doorgevoerd in de volgende producten die los van de afsprakenset worden gepubliceerd:
  

**[uitwisselprofiel ZIN - ODB](https://kik-v.gitlab.io/uitwisselprofielen/uitwisselprofiel-odb)**

- Verwijderd: Op de organisatorische laag stond bij in- en exclusiecriteria dat zorgaanbieders met minder dan tien cliënten zijn uitgezonderd van aanlevering van gegevens. Zij moeten wel aanleveren maar deze gegevens worden in kader van privacy en herleidbaarheid niet gepubliceerd. 
- Verwijderd: Op de organisatorische laag stond dat terugkoppeling plaatsvindt middels een individueel dashboard voor zorgaanbieders (zoals bij de MSZ). Een dashboard is niet haalbaar gebleken, maar alternatieven hiervoor worden op dit moment onderzocht.
- Gewijzigd: Op de laag Privacy en Informatiebeveiliging is het stuk over herleidbaarheid van gegevens bij zorgaanbieders met minder dan tien cliënten aangepast.
- Toegevoegd: op de semantische laag zijn bij de voorbeeldberekeningen, de validatiechecks die in het Desan portaal plaatsvinden, toegevoegd. 
- Gewijzigd: De beschrijvingen bij ‘Doel van de uitvraag' en ‘Doel per indicator’ zijn aangepast aan de hand van de laatste versie van de handboeken (versie 2021). Waaronder: medewerkers is gewijzigd in personeel.
- Gewijzigd: op de semantische laag zijn de ‘Algemene uitgangspunten’ bij de in- en exclusiecriteria aangepast.
- Het gebruik van de Totaalscore cliëntervaring en de verwijzing naar het kwaliteitsverslag zijn, conform nieuwe handboeken, toegevoegd aan het uitwisselprofiel.
- Gewijzigd: de concepten en de voorbeeldberekeningen waar naar verwezen wordt zijn aangepast aan de hand van de nieuwe handboeken van het Kwaliteitskader.

{{< /hint >}}