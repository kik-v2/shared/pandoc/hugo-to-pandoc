---
title: "Personeel"
weight: 10
---

# Personeelssamenstelling

Gegevens over personele samenstelling maken onderdeel uit van alle drie de gegevenssets.
  

In een tijdlijn ziet dit er als volgt uit:

![Uitwisselkalender personeel tijdlijn](uitwissel-kalender-personeel-tijdlijn.png)

De aanleveringen kennen de volgende overlap aan concepten:

![Uitwisselkalender personeel inhoud](uitwissel-kalender-personeel-inhoud.png)

Het is waarschijnlijk dat de concepten (en/of onderliggende gegevenselementen) personeelssamenstelling in de hierboven genoemde uitvragen herbruikbaar zijn op verschillende aanlevermomenten. Zo kunnen gegevens aangeleverd aan het zorgkantoor op 1 april mogelijk hergebruikt worden voor de aanlevering aan het CIBG voor de Jaarverantwoording Zorg en de aanlevering aan het Zorginstituut.
  

In de bovenstaande figuur is de overlap op conceptniveau in kaart gebracht. Afspraken over eventueel hergebruik worden vastgelegd in de uitwisselprofielen per afnemer.
  
  
In het uitwisselprofiel worden ook afspraken vastgelegd over terugkoppelmomenten. Dit kan terugkoppeling naar de individuele aanbieder zijn en/of terugkoppeling in de vorm van (openbare) datasets met gegevens van alle aanbieders. Op dit moment publiceert het Zorginstituut openbare datasets in juli T+1 en het CIBG in september T+1. Daarnaast publiceert het CIBG de afgeronde jaarverantwoordingen per instelling, na ontvangst.
