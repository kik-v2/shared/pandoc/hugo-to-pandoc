# PanDoc Configuratie

De PanDoc-configuratie om vanuit **HUGO SSG content** een **PDF** te genereren bestaat uit:

- Een **maatwerk-script** dat is ontwikkeld in *NodeJs* op basis van *TypeScript* waarmee:
  - De HUGO Markdown vanuit de standaard **file tree** structuur wordt voorbereid om te parsen als **R Markdown** (PanDoc input variant)
  - De relevante relevante **HUGO built-in ShortCodes** en **Book Theme ShortCodes** worden geconvereerd naar **R Markdown** of daarbinnen omgezet naar **LateX** statements.
- Een [HUHO ShortCode to PanDoc Mapping](hugo-short-code-to-pandoc-mapping.json) waarmee via :
  - De voor PDF-generatie relevante *HUGO built-in ShortCodes* en *Book Theme ShortCodes* worden vertaald naar:
    - *R Markdown* specefieke content
    - en daarbinnen waar nodig *LaTeX-statements*
- Een [LaTeX Template ](template.tex) waarop de volgende aanvulling op de standaard PanDoc template zijn toegevoegd:
  - Een **HugoShortCodeBegin** en **HugoShortCodeEnd** commando waarmee de start en einde van een te naar *LaTeX* vertaalde *HUGO ShortCede* wordt gemarkeerd. Dit is nodig om te zorgen dat daarbinnen de tekst als *R Mardown* wordt gezien.
  - Een **LaTeX tcolorbox**-environment voor de weergave van de HUGO **hint** ShortCode. Inclusief de *kleuren-definities* voor de weergave van de **info**, **warning** of **danger** varianten
- [PanDoc Defaults](defaults.yml) waarin:
  - De configuratie van het te genereren PDF-document staat
  - Vanuit de **HUGO file tree** structuur via een maatwerk TypeScript:
    - De locaties van de voorbereide **input-files** worden geparst
    - De **resource-path**(s) van de gebruikte figuren e.o. niet Markdown worden geparst

## HUGO SSG Markdown Content Vereisten

Vanuit de eerste testen zijn de volgende bevindingen gedaan waaraan de bron-content in HUGO Markdown moet voldoen om de conversie naar een PDF te kunnen uitvoeren.

> T.z.t. kan dat in een integratie test script worden opgenomen via een Markdown Lint functie (de NPM, Python, e.o. Ecosystemen bieden daar meerdere packages voor)

### Regelafstanden aanhouden

Gebruik regelafstanden tussen diverse Markdowwn onderdelen zoals lijsten, blokken etc.
  

Bijvoorbeeld bij een lijst in *Markdown*

```markdown

Lijst incorrect:

- item 1
- item 2
- item ...
- item n
Direct zonder regel ertussen

```

**Resulteert in een foutmelding** wanneer deze via PanDoc via LateX naar PDF wordt geconverteerd.
   

Correctie op bovenstaande voorbeeld

```markdown

Lijst correct:

- item 1
- item 2
- item ...
- item n

Met een regel ertussen

```
